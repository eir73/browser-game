import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
      tagName: 'div',
      className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const healthAttributes = {
      value: fighter.health,
      max: 60
  }
  const attackAttributes = {
      value: fighter.attack,
      max: 5
  }
  const defenseAttributes = {
      value: fighter.defense,
      max: 4
  }
  const fighterImage = createFighterImage(fighter);
  const fighterName = createElement({
    tagName: 'h2',
    className: 'fighter-preview___name'
  });
  const fighterStats = createElement({
    tagName: 'div',
    className: 'fighter-preview___stats'
  });
  const fighterHealthBar = createElement({
    tagName: 'progress',
    className: 'fighter-preview___stat',
    attributes: healthAttributes
  });
  const fighterAttackBar = createElement({
    tagName: 'progress',
    className: 'fighter-preview___stat',
    attributes: attackAttributes,
  });
  const fighterDefenseBar = createElement({
    tagName: 'progress',
    className: 'fighter-preview___stat',
    attributes: defenseAttributes
  });
  const fighterHealth = createElement({
      tagName: 'p'
  });
  const fighterAttack = createElement({
      tagName: 'p'
  });
  const fighterDefense = createElement({
      tagName: 'p'
  });
    
  fighterName.append(`${fighter.name}`.toUpperCase());
  fighterHealth.append('HEALTH: ', fighterHealthBar);
  fighterAttack.append('ATTACK: ', fighterAttackBar);
  fighterDefense.append('DEFENCE: ', fighterDefenseBar);
  fighterStats.append(fighterHealth, fighterAttack, fighterDefense);
  fighterElement.append(fighterName, fighterImage, fighterStats);
    
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
