import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
      // set eventListeners on keyboard (keydown and keyup)
      firstFighter.currentHealth = firstFighter.health;
      secondFighter.currentHealth = secondFighter.health;
      document.onkeydown = event => { handleKeyDown(event.code, firstFighter, secondFighter) };
      document.onkeyup = event => { handleKeyUp(event.code, firstFighter, secondFighter) };
      document.onkeypress = event => { checkHealth(firstFighter, secondFighter, resolve) };
  });
}

/* FLAGS is an object for cheking the state of pressed keys 
* true for pressed key, and false when keyup event is fired
* **CanCrit - values, that allows to use critical combination
* .block(key) blocks a value for 10 sec
*/
const FLAGS = {
  [controls.PlayerOneAttack] : false,
  [controls.PlayerOneBlock]: false,
  [controls.PlayerTwoAttack]: false,
  [controls.PlayerTwoBlock]: false,
  [controls.PlayerOneCriticalHitCombination[0]]: false,
  [controls.PlayerOneCriticalHitCombination[1]]: false,
  [controls.PlayerOneCriticalHitCombination[2]]: false,
  [controls.PlayerTwoCriticalHitCombination[0]]: false,
  [controls.PlayerTwoCriticalHitCombination[1]]: false,
  [controls.PlayerTwoCriticalHitCombination[2]]: false,
  'firstCanCrit': true,
  'secondCanCrit': true,
  'block': (key) => {
      FLAGS[key] = false;
      setTimeout(() => { FLAGS[key] = true; }, 10000);
  }
};

function checkHealth(firstFighter, secondFighter, callback) {
    if(getHealth(firstFighter) <= 0) {
        callback(secondFighter);
    } else if (getHealth(secondFighter) <= 0) {
        callback(firstFighter)
    }
}

function decreaseHealthBar(maxHealth, currentHealth, healthBar) {
    healthBar.style.width = currentHealth >= 0 ? parseFloat(currentHealth / maxHealth) * 100 + '%': '0%';
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
    
  return damage >= 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const hitPower = fighter.attack * criticalHitChance;
    
  return hitPower;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const blockPower = fighter.defense * dodgeChance;
    
  return blockPower;
}
    
function getHealth(fighter) {
    
  return fighter.currentHealth;
}

function setHealth(fighter, health, healthBar) {
  fighter.currentHealth = health;
  decreaseHealthBar(fighter.health, fighter.currentHealth, healthBar);
}

function getCriticalDamage(fighter) {
  const criticalDamage = fighter.attack * 2;
    
  return criticalDamage;
}
    
function handleKeyDown(keyCode, firstFighter, secondFighter) {
    if(FLAGS[keyCode] !== undefined) {
      FLAGS[keyCode] = true;
      let hitToFirst = 0;
      let hitToSecond = 0;
        
      if(keyCode === controls.PlayerOneBlock) {          //handle block keys
          FLAGS[controls.PlayerOneAttack] = false;
          FLAGS[controls.PlayerOneCriticalHitCombination[0]] = false;
          FLAGS[controls.PlayerOneCriticalHitCombination[1]] = false;
          FLAGS[controls.PlayerOneCriticalHitCombination[2]] = false;
      } else if(keyCode === controls.PlayerTwoBlock) {
          FLAGS[controls.PlayerTwoAttack] = false;
          FLAGS[controls.PlayerTwoCriticalHitCombination[0]] = false;
          FLAGS[controls.PlayerTwoCriticalHitCombination[1]] = false;
          FLAGS[controls.PlayerTwoCriticalHitCombination[2]] = false;
      } else if(keyCode === controls.PlayerOneAttack && !FLAGS[controls.PlayerOneBlock] && !FLAGS[controls.PlayerTwoBlock]) {
          //first fighter hit second fighter
          hitToSecond = getDamage(firstFighter, secondFighter);
      } else if(keyCode === controls.PlayerTwoAttack && !FLAGS[controls.PlayerOneBlock] && !FLAGS[controls.PlayerTwoBlock]) {
          //second fighter hit first fighter
          hitToFirst = getDamage(secondFighter, firstFighter);
      } else if(FLAGS.firstCanCrit && 
                FLAGS[controls.PlayerOneCriticalHitCombination[0]] &&
                FLAGS[controls.PlayerOneCriticalHitCombination[1]] &&
                FLAGS[controls.PlayerOneCriticalHitCombination[2]]) {
          //first fihter hit critical to second
          hitToSecond = getCriticalDamage(firstFighter);
          FLAGS.block('firstCanCrit');
      } else if(FLAGS.secondCanCrit && 
                FLAGS[controls.PlayerTwoCriticalHitCombination[0]] &&
                FLAGS[controls.PlayerTwoCriticalHitCombination[1]] &&
                FLAGS[controls.PlayerTwoCriticalHitCombination[2]]) {
          //second fighter hit critical to first
          hitToFirst = getCriticalDamage(secondFighter);
          FLAGS.block('secondCanCrit');
      }
        
      const firstHP = getHealth(firstFighter);
      const secondHP = getHealth(secondFighter);
      const firstFighterHealthBar = document.getElementById('left-fighter-indicator');
      const secondFighterHealthBar = document.getElementById('right-fighter-indicator');
      setHealth(firstFighter, firstHP - hitToFirst, firstFighterHealthBar);
      setHealth(secondFighter, secondHP - hitToSecond, secondFighterHealthBar);   
    } 
}
    
function handleKeyUp(keyCode) {
    if(FLAGS[keyCode] !== undefined) {
      FLAGS[keyCode] = false;
        
      if(keyCode === controls.PlayerOneBlock) {
        FLAGS[controls.PlayerOneAttack] = false;
      } else if(keyCode === controls.PlayerTwoBlock) {
          FLAGS[controls.PlayerTwoAttack] = false;
      }
    }
}
