import { showModal } from './modal'
import App from '../../app';
import { fighterService } from '../../services/fightersService';
import { createFighters } from '../../components/fightersView';

export function showWinnerModal(fighter) {
  // call showModal function 
    // fighter is a HTML-Element div>(p + img)
    showModal({ 
      title: 'WINNER!!!',
      bodyElement: fighter,
      onClose: async () => {
       // const fighters = await fighterService.getFighters();
       // const fightersElement = createFighters(fighters);
        const root = document.getElementById('root');
        root.innerHTML = '';
      //  root.appendChild(fightersElement);
          new App();
      }
    });
}
